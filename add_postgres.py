import logging
import json
import requests
import sys

"""
Run This Script:

    lzd add_postgres.py <shopee or lazada> <2 char Country Code>
"""

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)
log = open('postlog.log', 'a')

channel = 'lazada'
country = 'TH'
files = f'seller_to_migrate/seller-{channel}-{country.lower()}.json'

success_seller = []
failed_seller = []

with open(files, 'r') as f:
    datas = json.load(f)

for data in datas:
    url = 'http://prod-productcatalog-1b-0.acommerce.platform:8061/internal/sellers/'
    # url = 'http://sellermaster-dev.acommercedev.service/internal/sellers/'
    r = requests.post(url, json=data)
    if r.status_code != 201:
        logger.info('Fail: %s' % data['channelSellerId'])
        log.write('Fail: %s' % data['channelSellerId'])
        log.write(f'{r.status_code} : {r.text}\n')
        log.write(json.dumps(data))
        log.write("\n\n")

        failed_seller.append(data)
    else:
        logger.info('Success: %s' % data['channelSellerId'])
        log.write('Success: %s' % data['channelSellerId'])
        log.write("\n\n")

        resp = r.json()
        success_seller.append(resp)

# Save all success seller information
with open(f'new_registered_seller/seller-lazada-{country.lower()}.json', 'w') as outfile:
    json.dump(success_seller, outfile, indent=4)

# Save all failed seller information.
with open(f'existing_seller/seller-lazada-{country.lower()}.json', 'w') as outfile:
    json.dump(failed_seller, outfile, indent=4)



