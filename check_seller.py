import psycopg2
import logging
import json
import sys
import csv
import requests

log = open(f'update_cred_log/check_not_exist_seller.log', 'w')

try:
    # conn = psycopg2.connect(
    #     "dbname='seller_master' user='seller_master_user' host='seller-master-db.acommerce.platform' password='xssW9rcRA7tQ5PvW' port='5432'")
    conn = psycopg2.connect(
        "dbname='seller_master_dev' user='seller_master_user' host='seller-master-dev-db.acommercedev.platform' password='uVuUdvrK3DA8eFtV' port='5432'")
except Exception as e:
    print(e)


cur = conn.cursor()
cur.execute("SELECT c.channel_id, s.channel_seller_id, p.partner_id, p.name \
    FROM public.sellers_seller as s ,public.sellers_seller_partners as sp, public.partners_partner as p, public.channels_channel as c \
    where p.id = sp.partner_id \
	    and sp.seller_id = s.id \
	    and c.id = s.channel_id \
	    and s.channel_seller_password is null \
	    and s.is_active = true;")

rows = cur.fetchall()
for row in rows:
    log.write(f'{row[0]},{row[3]},{row[2]},{row[1]}\n')