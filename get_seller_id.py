from lzd import lazop
from bson.objectid import ObjectId
from pymongo import MongoClient
import logging
import sys




def get_lazada_seller(client, access_token):
    request = lazop.LazopRequest('/seller/get', 'GET')
    request.add_api_param('access_token', access_token)

    response = client.execute(request)

    return response


def get_partner_sellers(country):
    mongo_uri_by_country = {
        'th': f"""
                mongodb://cm_bridge_multi_sellers_user:at4A4bHDspdVft3g@prod-mongo-cluster-1a-0.acommerce.platform,prod-mongo-cluster-1b-0.acommerce.platform/bridge_lazada_th_multi_sellers?replicaSet=prodMongo&authSource=admin
              """,
        'id': """
                mongodb://cm_bridge_multi_sellers_user:at4A4bHDspdVft3g@prod-mongo-cluster-1a-0.acommerce.platform,prod-mongo-cluster-1b-0.acommerce.platform/bridge_lazada_id_multi_sellers?replicaSet=prodMongo&authSource=admin
              """,
        'ph': """
                mongodb://cm_bridge_multi_sellers_user:at4A4bHDspdVft3g@prod-mongo-cluster-1a-0.acommerce.platform,prod-mongo-cluster-1b-0.acommerce.platform/bridge_lazada_ph_multi_sellers?replicaSet=prodMongo&authSource=admin
              """,
        'sg': """
                mongodb://cm_bridge_multi_sellers_user:at4A4bHDspdVft3g@prod-mongo-cluster-1a-0.acommerce.platform,prod-mongo-cluster-1b-0.acommerce.platform/bridge_lazada_sg_multi_sellers?replicaSet=prodMongo&authSource=admin
              """,
        'my': """
                mongodb://cm_bridge_multi_sellers_user:at4A4bHDspdVft3g@prod-mongo-cluster-1a-0.acommerce.platform,prod-mongo-cluster-1b-0.acommerce.platform/bridge_lazada_my_multi_sellers?replicaSet=prodMongo&authSource=admin
              """,
    }

    try:
        uri = mongo_uri_by_country[country].strip()
        client = MongoClient(uri)
        db = client[f'bridge_lazada_{country}_multi_sellers']

        partner_sellers = db.partners.aggregate([{
            "$lookup": {
                "from": "sellers",
                "localField": "channels.0.sellers.0",
                "foreignField": "_id",
                "as": "sellers"
            }
        }])

        return partner_sellers

    except Exception as e:
        print(e)

    finally:
        client.close()


def build_client(url, app_key, app_secret):
    client = lazop.LazopClient(url, app_key, app_secret)

    return client

def update_mongo(seller_id, lazada_seller_id):
    mongo_uri_by_country = {
        'th': f"""
                mongodb://cm_bridge_multi_sellers_user:at4A4bHDspdVft3g@prod-mongo-cluster-1a-0.acommerce.platform,prod-mongo-cluster-1b-0.acommerce.platform/bridge_lazada_th_multi_sellers?replicaSet=prodMongo&authSource=admin
              """,
        'id': """
                mongodb://cm_bridge_multi_sellers_user:at4A4bHDspdVft3g@prod-mongo-cluster-1a-0.acommerce.platform,prod-mongo-cluster-1b-0.acommerce.platform/bridge_lazada_id_multi_sellers?replicaSet=prodMongo&authSource=admin
              """,
        'ph': """
                mongodb://cm_bridge_multi_sellers_user:at4A4bHDspdVft3g@prod-mongo-cluster-1a-0.acommerce.platform,prod-mongo-cluster-1b-0.acommerce.platform/bridge_lazada_ph_multi_sellers?replicaSet=prodMongo&authSource=admin
              """,
        'sg': """
                mongodb://cm_bridge_multi_sellers_user:at4A4bHDspdVft3g@prod-mongo-cluster-1a-0.acommerce.platform,prod-mongo-cluster-1b-0.acommerce.platform/bridge_lazada_sg_multi_sellers?replicaSet=prodMongo&authSource=admin
              """,
        'my': """
                mongodb://cm_bridge_multi_sellers_user:at4A4bHDspdVft3g@prod-mongo-cluster-1a-0.acommerce.platform,prod-mongo-cluster-1b-0.acommerce.platform/bridge_lazada_my_multi_sellers?replicaSet=prodMongo&authSource=admin
              """,
    }

    
    try:
        uri = mongo_uri_by_country[country].strip()
        client = MongoClient(uri)
        db = client[f'bridge_lazada_{country}_multi_sellers']

        sellers = db.sellers.update(
            {"_id":ObjectId(str(seller_id))},
            {"$set" : {"lazadaSellerId": str(lazada_seller_id)}})
        return sellers

    except Exception as e:
        print(e)

    finally:
        client.close()



if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG)
    logger = logging.getLogger(__name__)
    log = open('get_seller_id.log','a')

    url_by_country = {
        'th': 'https://api.lazada.co.th/rest',
        'id': 'https://api.lazada.co.id/rest',
        'ph': 'https://api.lazada.com.ph/rest',
        'sg': 'https://api.lazada.sg/rest',
        'my': 'https://api.lazada.com.my/rest'
    }
    app_key = '100525'
    app_secret = 'HlpyqD0ZYPboQzXHiyUASLOnCXnrqGwx'

    country = 'ID'
    country = country.lower()
    client = build_client(url_by_country[country], app_key, app_secret)

    partner_sellers = get_partner_sellers(country)
    for partner in partner_sellers:
        try:
            seller = partner['sellers'][0]
            if 'lazadaSellerId' not in seller:
                lazada_seller = get_lazada_seller(
                    client, partner['sellers'][0]['accessToken'])
                lazada_seller_id = lazada_seller.body['data']['seller_id']
                lazada_short_code = lazada_seller.body['data']['short_code']
                lazada_seller_name = lazada_seller.body['data']['name']
                lazada_seller_email = lazada_seller.body['data']['email']

                seller_id = partner['sellers'][0]['_id']
                partner_id = partner['partnerId']

                logger.debug(
                    f'"{seller_id}", "{partner_id}", "{lazada_seller_id}", "{lazada_short_code}", "{lazada_seller_name}", "{lazada_seller_email}"'
                )
                log.write(
                    f'{seller_id},{partner_id},{lazada_seller_id},{lazada_short_code},{lazada_seller_name},{lazada_seller_email}\n'
                )

                ret_val = update_mongo(seller_id, lazada_seller_id)
                logger.debug(ret_val)
                log.write(f'{ret_val}\n')
            else:
                logger.debug(
                    f'"{seller["shortCode"]}" already has lazadaSellerId.'
                )

        except (KeyError, IndexError) as e:
            try:
                logger.debug(f"No partner-seller association for partner: {partner['partnerId']}")
                # log.write(f"No partner-seller association for partner: {partner['partnerId']}\n")
            except KeyError:
                logger.debug(f"No partner-seller association for partner")
            pass