#!/usr/bin/env lzd
# -*- coding: utf-8 -*-

from setuptools import setup, find_packages

setup(
    name='lazop-sdk-lzd',
    version='1.1.0',
    author='top',
    author_email='xuteng.xt@alibaba-inc.com',
    packages=find_packages(),
    install_requires=[],
    license='MIT',
)

