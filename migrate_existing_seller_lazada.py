import pymongo
import logging
import json
import sys
import psycopg2

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)
log = open('lazada_existing_seller_migration.log', 'a')

"""
Run this script:

    lzd migrate_existing_seller_lazada.py <Channel ID> <2 char Country Code>
"""
channel_id = str(sys.argv[1])
country = str(sys.argv[2])

# db_url = f'mongodb://cm_bridge_multi_sellers_user:at4A4bHDspdVft3g@prod-mongo-cluster-1a-0.acommerce.platform,prod-mongo-cluster-1b-0.acommerce.platform/bridge_lazada_{country.lower()}_multi_sellers?replicaSet=prodMongo&authSource=admin'
db_url = f'mongodb://cm_bridge_multi_sellers_user:at4A4bHDspdVft3g@dev-mongo-1a-0.acommercedev.platform:27017/bridge_lazada_{country.lower()}_multi_sellers'
myclient = pymongo.MongoClient(db_url)
db = myclient[f'bridge_lazada_{country.lower()}_multi_sellers']

files = f'existing_seller/seller-lazada-{country.lower()}.json'

with open(files, 'r') as f:
    datas = json.load(f)

try:
    # conn = psycopg2.connect(
    #     "dbname='seller_master' user='seller_master_user' host='seller-master-db.acommerce.platform' password='xssW9rcRA7tQ5PvW' port='5432'")
    conn = psycopg2.connect(
        "dbname='seller_master_dev' user='seller_master_user' host='seller-master-dev-db.acommercedev.platform' password='uVuUdvrK3DA8eFtV' port='5432'")
except Exception as e:
    print(e)
    raise

short_code_list = [data['channelSellerId'] for data in datas]

sellers = db.sellers.find(
    {
        'shortCode': {'$in': short_code_list},
    }
)

if not sellers.alive:
    raise Exception("No seller is found.")

if sellers.count() != len(short_code_list):
    raise Exception("Size of seller list is not the same.")

for data in datas:
    updated_dict = dict()
    cur = conn.cursor()
    cur.execute(f"SELECT seller.seller_id, "
                "seller.name, "
                "seller.channel_seller_id, "
                "seller.country, "
                "seller.is_active, "
                "seller.is_deleted , "
                "seller.channel_seller_account, "
                "seller.updated_time "
                "FROM sellers_seller AS seller "
                "INNER JOIN channels_channel AS channel "
                "ON channel.id = seller.channel_id "
                "AND channel.channel_id = '%s' "
                "WHERE seller.country = '%s' "
                "AND seller.channel_seller_id = '%s';" % (data['channelId'], data['country'].upper(), data['channelSellerId'])
                )

    rows = cur.fetchall()
    for row in rows:
        updated_dict['sellerId'] = row[0]
        updated_dict['name'] = row[1]
        updated_dict['channelSellerId'] = row[2]
        updated_dict['country'] = row[3]
        updated_dict['active'] = row[4]
        updated_dict['deleted'] = row[5]
        updated_dict['channelSellerAccount'] = row[6]
        updated_dict['updatedTime'] = row[7]
        updated_dict['partnerIds'] = data['partnerIds']
        updated_dict['_cls'] = 'SellerDocument.MultiSellerDocument'

        try:
            db.sellers.update_one(
                {
                    'shortCode': data['channelSellerId']
                },
                {
                    '$set': updated_dict
                }
            )

            logger.info('Success: %s' % updated_dict['channelSellerId'])
            log.write('Success: %s' % updated_dict['channelSellerId'])
        except Exception as e:
            logger.exception('Failed: %s' % updated_dict['channelSellerId'])
            log.write('Failed: %s' % updated_dict['channelSellerId'])
            log.write('Exception: %s' % repr(e))
