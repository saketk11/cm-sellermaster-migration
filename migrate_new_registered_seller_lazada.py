import pymongo
import logging
import json
import sys

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)
log = open('lazada_new_seller_migration.log', 'a')

"""
Run this script:

    lzd migrate_new_registered_seller_lazada.py <2 char Country Code>
"""

# country = str(sys.argv[1])
country = 'MY'

# db_url = f'mongodb://cm_bridge_multi_sellers_user:at4A4bHDspdVft3g@prod-mongo-cluster-1a-0.acommerce.platform,prod-mongo-cluster-1b-0.acommerce.platform/bridge_lazada_{country.lower()}_multi_sellers?replicaSet=prodMongo&authSource=admin'
db_url = f'mongodb://cm_bridge_multi_sellers_user:at4A4bHDspdVft3g@dev-mongo-1a-0.acommercedev.platform:27017/bridge_lazada_{country.lower()}_multi_sellers'
myclient = pymongo.MongoClient(db_url)
db = myclient[f'bridge_lazada_{country.lower()}_multi_sellers']
success = 0
fail = 0

files = f'new_registered_seller/seller-lazada-{country.lower()}.json'

with open(files, 'r') as f:
    datas = json.load(f)

for data in datas:
    seller_id = data['id']
    channel_id = data['channelId']
    partner_ids = data['partnerIds']
    country = data['country']
    name = data['name']
    deleted = data['isDeleted']
    short_code = data['channelSellerId']

    try:
        db.sellers.update_one(
            {
                'shortCode': short_code,
                '_cls': {'$exists': False}
            },
            {
                '$set':
                    {
                        'sellerId': seller_id,
                        'channelId': channel_id,
                        'partnerIds': partner_ids,
                        'country': country,
                        '_cls': 'SellerDocument.MultiSellerDocument',
                        'deleted': deleted
                    }
            }
        )

        logger.info('Success: %s' % data['channelSellerId'])
        log.write('Success: %s' % data['channelSellerId'])
    except Exception as e:
        logger.exception('Failed: %s' % data['channelSellerId'])
        log.write('Failed: %s' % data['channelSellerId'])
        log.write('Exception: %s' % repr(e))
