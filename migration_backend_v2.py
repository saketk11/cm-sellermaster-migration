import psycopg2
import logging
import json
import sys
import requests
logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)
log_f = open('v2_migratesellers_fail.log', 'a')
log_s = open('v2_migratesellers_success.log', 'a')


"""
Run this script:

    lzd migration_backend_v2.py
"""
try:
    conn_v2 = psycopg2.connect("dbname='heimdall' user='heimdall_user' host='heimdall_id.acommerce.platform' password='DUjKertV5xao'")
except Exception as e:
    print(e)
cur = conn_v2.cursor()
cur.execute(f"SELECT store.name, store.valkyrie_auth->'shopid', store.active, store.created_at, store.updated_at, channel.cpms_channel_id, partner.cpms_id, store.id \
            FROM public.stores_store as store ,public.channels_channel as channel, public.partners_partner as partner \
            where store.channel_id in ('10','15','12','3') \
                and channel.id = store.channel_id \
                and partner.id = store.partner_id \
                and store.active = true;")

rows = cur.fetchall()
count = 0
sellermaster_not_exist = []
success_sellers = []

for row in rows:
    sellerdata = {}
    sellerdata['channelId'] = row[5]
    sellerdata['channelSellerId'] = row[1]
    sellerdata['name'] = row[0]
    sellerdata['isActive'] = row[2]
    sellerdata['partnerId'] = row[6]

    parameters = {
        "partner": sellerdata['partnerId'],
        "channel": sellerdata['channelId'],
        "name": sellerdata['name']}

    url = f'http://prod-productcatalog-1b-0.acommerce.platform:8061/sellers/'
    r = requests.get(url, params=parameters)
    data = r.json()
    if len(data) == 0:
        sellermaster_not_exist.append(sellerdata)
        log_f.write(f'{str(sellerdata)}\n')
    else:
        seller = data[0]
        sellerdata['cpms_id'] = seller['id']
        success_sellers.append(sellerdata)
        cur.execute(f"UPDATE stores_store SET cpms_id = '{sellerdata['cpms_id']}' WHERE id = {row[7]};") 
        conn_v2.commit()
        log_s.write(f'{str(sellerdata)}\n')







