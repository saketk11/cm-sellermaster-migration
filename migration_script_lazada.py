import pymongo
import logging
import json
import sys

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)
log = open('migratelog.log', 'a')

"""
Run this script:

    lzd migration_script_lazada.py <2 char Country Code>
"""

# country = str(sys.argv[1])
country = 'TH'
db_url = f'mongodb://cm_bridge_multi_sellers_user:at4A4bHDspdVft3g@prod-mongo-cluster-1a-0.acommerce.platform,prod-mongo-cluster-1b-0.acommerce.platform/bridge_lazada_{country.lower()}_multi_sellers?replicaSet=prodMongo&authSource=admin'
# db_url = f'mongodb://cm_bridge_multi_sellers_user:at4A4bHDspdVft3g@dev-mongo-1a-0.acommercedev.platform:27017/bridge_lazada_th_multi_sellers'
myclient = pymongo.MongoClient(db_url)
db = myclient[f'bridge_lazada_{country.lower()}_multi_sellers']
sellers = db.sellers.find()
sellerfin = []
success = 0
fail = 0

for row in sellers:
    try:
        sellerdata = {}
        sellerdata['id'] = str(row['_id'])
        
        channel = db.channels.find(
            {}, {'channelId': 1, '_id': 0})
        for row1 in channel[:1]:
            channelId = row1['channelId']
        
        sellerdata['channelId'] = channelId
        sellerdata['channelSellerId'] = row['shortCode']
        sellerdata['channelSellerAccount'] = row['account']
        try:
            sellerdata['name'] = row['name']
        except Exception:
            log.write('Seller with no name and partner: {0} country: {1}     {2}\n'.format(
                row['account'], country, row['_id']))
            logger.debug('Seller with no name and partner: {0} country: {1}'.format(
                row['account'], country))

        sellerdata['country'] = country.upper()
        sellerdata['isActive'] = row['active']
        try:
            sellerdata['channelDetail'] = {
                "lazadaSellerId": row['lazadaSellerId']
                }
        except Exception:
            log.write('Seller with no lazadasellerid: {0} country: {1}     {2}\n'.format(
                row['account'], country, row['_id']))
            logger.debug('Seller with no lazadasellerid: {0} country: {1}'.format(
                row['account'], country))

        partners = db.partners.aggregate([
            {'$unwind': '$channels'},
            {'$unwind': '$channels.sellers'},
            {'$match': {'channels.sellers': row['_id'], }},
            {'$project':
             {'partnerId': 1, '_id': 0}},
        ])

        partnerdata = []
        for line in partners:
            partnerdata.append(line['partnerId'])

        if not partnerdata:
            log.write('Seller with no partner(s): {0} country: {1}     {2}\n'.format(
                row['account'], country, row['_id']))
            log.debug('Seller with no partner(s): {0} country: {1}'.format(
                row['account'], country))
            fail = + 1
        else:
            if sellerdata['isActive'] == False:
                logger.debug("Seller: {0} partner: {1}".format(
                    sellerdata['id'], partnerdata))
            sellerdata['partnerIds'] = partnerdata
            sellerfin.append(sellerdata)
            success = success + 1

    except Exception as e:
        logger.debug(row['account'])
        fail = fail + 1


print(f'Success {success} , Fail {fail}')

with open(f'seller_to_migrate/seller-lazada-{country}.json', 'w') as outfile:
    json.dump(sellerfin, outfile, indent=4)
