import psycopg2
import logging
import json
import sys
logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)
log = open('migratelog.log', 'a')


"""
Run this script:

    lzd migration_script_shopee.py <2 char Country Code>
"""

country = str(sys.argv[1])

try:
    conn = psycopg2.connect("dbname='heimdall' user='heimdall_user' host='heimdall_id.acommerce.platform' password='DUjKertV5xao'")
except Exception as e:
    print(e)

channel = ""
if country == "TH": channel = "10"
if country == "PH": channel = "15"
if country == "SG": channel = "12"
if country == "ID": channel = "3"


cur = conn.cursor()
cur.execute(f"SELECT store.name, store.valkyrie_auth->'shopid', store.active, store.created_at, store.updated_at, channel.cpms_channel_id, partner.cpms_id \
            FROM public.stores_store as store ,public.channels_channel as channel, public.partners_partner as partner \
            where store.channel_id in ({channel}) \
                and channel.id = store.channel_id \
                and partner.id = store.partner_id \
                and store.active = true;")

rows = cur.fetchall()
sellerfin = []
for row in rows:
    sellerdata = {}
    sellerdata['channelId'] = row[5]
    sellerdata['channelSellerId'] = row[1]
    sellerdata['name'] = row[0]
    sellerdata['country'] = country
    sellerdata['isActive'] = row[2]
    sellerdata['partnerIds'] = [f'{row[6]}']
    sellerfin.append(sellerdata)

with open(f"seller_to_migrate/seller-shopee-{country}.json","w") as outfile:
    json.dump(sellerfin,outfile,indent=4)