import psycopg2
import logging
import json
import sys
import csv
import requests
logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)

"""
Run This Script:

    lzd upload_seller_cred.py <channel_id> <shortcode of channel folder>
"""


channel = str(sys.argv[1])
log = open(f'log_cred/updatecredlog-{channel}.log', 'w')
log1 = open(f'update_cred_log/upload_cred_post.log', 'a')


try:
    conn = psycopg2.connect(
        "dbname='seller_master' user='seller_master_user' host='seller-master-db.acommerce.platform' password='xssW9rcRA7tQ5PvW' port='5432'")
except Exception as e:
    print(e)

country = ""
if channel == 'lazada-th' or channel == 'shopeeth':
    country = "TH"
if channel == '262':
    country = "MY"
if channel == 'lazada-sg' or channel == 'shopeesg':
    country = "SG"
if channel == 'lazada-id' or channel == 'shopee':
    country = "ID"
if channel == 'lazada-ph' or channel == 'shopeeph':
    country = "PH"

code = str(sys.argv[2])
if code == "lzd":
    market = "Lazada"
if code == "shp":
    market = "Shopee"

csvDataFile = open(
    f'seller_sheet_{code}/CPMS Partners Seller Access - {market} {country}.csv')
csvReader = csv.reader(csvDataFile, delimiter=',')
next(csvReader)
for rowcsv in csvReader:
    cur = conn.cursor()
    cur.execute(f"SELECT s.seller_id, c.channel_id, s.channel_seller_id, p.partner_id, s.name, s.country, s.is_active, s.is_deleted \
    FROM public.sellers_seller as s ,public.sellers_seller_partners as sp, public.partners_partner as p, public.channels_channel as c \
    where p.id = sp.partner_id \
	    and sp.seller_id = s.id \
	    and c.id = s.channel_id \
	    and c.channel_id = '{channel}' \
	    and p.partner_id = '{rowcsv[0]}';")

    rows = cur.fetchall()
    if not rows:
        log.write(
            f"channel: {channel} | partnerId: {rowcsv[0]} | partnerName: {rowcsv[1]} | cannot find data on sellermaster.\n")
    for row in rows:
        # print(row)
        postdata = {}
        postdata['id'] = row[0]
        postdata['channelId'] = row[1]
        postdata['channelSellerId'] = row[2]
        postdata['channelSellerAccount'] = rowcsv[2]
        postdata['channelSellerPassword'] = rowcsv[3]
        postdata['partnerIds'] = [f"{row[3]}"]
        postdata['name'] = row[4]
        postdata['country'] = row[5]
        postdata['isActive'] = row[6]
        postdata['isDeleted'] = row[7]
        print(postdata)

        url = 'http://prod-productcatalog-1b-0.acommerce.platform:8061/internal/sellers/{0}/'.format(postdata['id'])
        r = requests.put(url, json = postdata)
        log1.write(f'{r.status_code} : {r.text}\n')
        log1.write(json.dumps(postdata))
        log1.write("\n\n")


